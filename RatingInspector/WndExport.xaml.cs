﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Microsoft.Win32;
using OfficeOpenXml;
using OfficeOpenXml.Table;

namespace RatingInspector
{
	/// <summary>
	/// Interaction logic for WndExport.xaml
	/// </summary>
	public partial class WndExport : Window
	{
		public WndExport()
		{
			InitializeComponent();
			LbCategories.ItemsSource=DbManager.Local.Categories;
			CbName.SelectionChanged += CbName_SelectionChanged;
		}

		private void Btn_Cancel_Click(object sender, RoutedEventArgs e)
		{
			Close();
		}
		private static TChildItem FindVisualChild<TChildItem>(DependencyObject obj)
			where TChildItem : DependencyObject
		{
			for (var i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
			{
				var child = VisualTreeHelper.GetChild(obj, i);
				if (child is TChildItem item)
					return item;
				var childOfChild = FindVisualChild<TChildItem>(child);
				if (childOfChild != null)
					return childOfChild;
			}
			return null;
		}
		private async void Btn_export_Click(object sender, RoutedEventArgs e)
		{
			var dlg = new SaveFileDialog
			{
				FileName = "Rtg"+DateTime.Today.ToShortDateString(),
				Filter = "*.xlsx|*.xlsx",
				InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
			};
			if (!dlg.ShowDialog() == true) return;
			var filepath = dlg.FileName;
			var list = await DbManager.Local.GetProfiles();
			if (CbName.SelectedIndex != 2)
				list.ForEach(p=> p.FillExportName(CbName.SelectedIndex));
			
			var xlsx = new ExcelPackage(new FileInfo(filepath));
			var ws = xlsx.Workbook.Worksheets.Add("RTG-" + DateTime.Today.Day + "-" + DateTime.Today.Month + "-" +
			                                      DateTime.Today.Year);
			//Headers
			var headersFlags = new Dictionary<string, CExportHeader>
			{
				{"RcfId", new CExportHeader{IsNeeded = CbRid.IsChecked??false, Title = "РШФ Id"}},
				{"FideId", new CExportHeader{IsNeeded = CbFid.IsChecked??false, Title = "FIDE Id"} },
				{"NameRu", new CExportHeader{IsNeeded = CbSort.SelectedIndex==2, Title = "Имя (Ru)"} },
				{"NameEng", new CExportHeader{IsNeeded = CbSort.SelectedIndex==2, Title = "Имя (En)"}  },
				{"NameExp", new CExportHeader{IsNeeded = CbSort.SelectedIndex!=2, Title = "Имя"}  },
				{"Fed", new CExportHeader{IsNeeded = CbCountry.IsChecked??false, Title = "Страна"} },
				{"Birth", new CExportHeader{IsNeeded = CbBirth.IsChecked??false, Title = "Г.Р."} },
				{"Reg", new CExportHeader{IsNeeded = CbRegion.IsChecked??false, Title = "Регион"} },
				{"FRtg",new CExportHeader{IsNeeded = CbFrtg.IsChecked??false, Title = "Fide Rtg"} },
				{"FRpd",new CExportHeader{IsNeeded = CbFrpd.IsChecked??false, Title = "Fide Rpd"} },
				{"FBlz",new CExportHeader{IsNeeded = CbFblz.IsChecked??false, Title = "Fide Blz"} },
				{"RRtg",new CExportHeader{IsNeeded = CbRrtg.IsChecked??false, Title = "РШФ Rtg"} },
				{"RRpd",new CExportHeader{IsNeeded = CbRrpd.IsChecked??false, Title = "РШФ Rpd"} },
				{"RBlz",new CExportHeader{IsNeeded = CbRblz.IsChecked??false, Title = "РШФ Blz"} },
				{"Cat", new CExportHeader{IsNeeded = CbGroup.IsChecked??false, Title = "Группа"}}
			};
			var col = 1;
			foreach (var header in headersFlags)
			{
				if (!header.Value.IsNeeded) continue;
				ws.Cells[1, col].Value = header.Value.Title;
				col++;
			}
			var sel = (from object item in LbCategories.SelectedItems select (ListBoxItem) (LbCategories.ItemContainerGenerator.ContainerFromItem(item)) into tListBoxItem select FindVisualChild<ContentPresenter>(tListBoxItem) into myContentPresenter let myDataTemplate = myContentPresenter.ContentTemplate select (TextBlock) myDataTemplate.FindName("Key", myContentPresenter) into myTextBlock select int.Parse(myTextBlock.Text)).ToList();
			list = list.Where(l => sel.Contains(l.Category)).ToList();
			switch (CbSort.SelectedIndex)
			{
				case 0:
					list = list.OrderBy(p => p.Category).ThenBy(p => p.ExportName).ToList();
					break;
				case 1:
					list = list.OrderBy(p => p.ExportName).ToList();
					break;
				default:
					list = list.OrderBy(p => p.Category).ToList();
					break;
			}
			var row = 2;
			var cats = DbManager.Local.Categories;
			//data
			foreach (var profile in list)
			{
				col = 1;
				if (headersFlags["RcfId"].IsNeeded)
				{
					ws.Cells[row, col].Value = profile.RcfId;
					col++;
				}
				if (headersFlags["FideId"].IsNeeded)
				{
					ws.Cells[row, col].Value = profile.FideId;
					col++;
				}
				if (headersFlags["NameRu"].IsNeeded)
				{
					ws.Cells[row, col].Value = profile.NameRu;
					col++;
				}
				if (headersFlags["NameEng"].IsNeeded)
				{
					ws.Cells[row, col].Value = profile.NameEng;
					col++;
				}
				if (headersFlags["NameExp"].IsNeeded)
				{
					ws.Cells[row, col].Value = profile.ExportName;
					col++;
				}
				if (headersFlags["Fed"].IsNeeded)
				{
					ws.Cells[row, col].Value = profile.Fed;
					col++;
				}
				if (headersFlags["Birth"].IsNeeded)
				{
					ws.Cells[row, col].Value = profile.Birth;
					col++;
				}
				if (headersFlags["Reg"].IsNeeded)
				{
					ws.Cells[row, col].Value = profile.Region;
					col++;
				}
				if (headersFlags["FRtg"].IsNeeded)
				{
					ws.Cells[row, col].Value = profile.FideRtg;
					col++;
				}
				if (headersFlags["FRpd"].IsNeeded)
				{
					ws.Cells[row, col].Value = profile.FideRpd;
					col++;
				}
				if (headersFlags["FBlz"].IsNeeded)
				{
					ws.Cells[row, col].Value = profile.FideBlz;
					col++;
				}
				if (headersFlags["RRtg"].IsNeeded)
				{
					ws.Cells[row, col].Value = profile.RcfRtg;
					col++;
				}
				if (headersFlags["RRpd"].IsNeeded)
				{
					ws.Cells[row, col].Value = profile.RcfRpd;
					col++;
				}
				if (headersFlags["RBlz"].IsNeeded)
				{
					ws.Cells[row, col].Value = profile.RcfBlz;
					col++;
				}
				if (headersFlags["Cat"].IsNeeded)
				{
					ws.Cells[row, col].Value = cats[profile.Category];
				}
				PbExport.Value = (row - 1) / list.Count *100;
				row++;
			}
			var range = ws.Cells[ws.Dimension.Start.Row, ws.Dimension.Start.Column, ws.Dimension.End.Row, ws.Dimension.End.Column];
			var table = ws.Tables.Add(range, "table1");
			range.AutoFitColumns();
			table.TableStyle = TableStyles.Light1;
			xlsx.Save();
			System.Diagnostics.Process.Start(filepath);
		}

		private void CbName_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (CbName.SelectedIndex != 2)
			{
				CbSort.IsEnabled = true;
				return;
			}
			CbSort.SelectedIndex = 2;
			CbSort.IsEnabled = false;
		}
	}
}
