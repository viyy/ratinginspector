﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using RatingInspector.DB.Models;

namespace RatingInspector
{
    /// <summary>
    /// Логика взаимодействия для WndProfilesView.xaml
    /// </summary>
    public partial class WndProfilesView : Window
    {
        private List<MProfile> _profiles = new List<MProfile>();
        private List<MProfile> _filtered = new List<MProfile>();
        public WndProfilesView()
        {
            InitializeComponent();
            listView.SelectionMode = SelectionMode.Single;
            Init();
        }
        private async void Init()
        {
            _profiles = await DbManager.Local.GetProfiles();
            _filtered = _profiles;
            var t = (_filtered.Join(DbManager.Local.Categories, p => p.Category, ct => ct.Key, (p, ct) => new
            {
                p.RcfId,
                p.FideId,
                p.NameEng,
                p.NameRu,
                p.Birth,
                Category = ct.Value
            })).ToList();
            listView.ItemsSource = t;
        }

        private void BtnSearch_Click(object sender, RoutedEventArgs e)
        {
            _filtered = _profiles.Where(p => (p.RcfId.ToString().StartsWith(TbSearch.Text) ||
                                            p.FideId.ToString().StartsWith(TbSearch.Text) || p.NameEng.StartsWith(TbSearch.Text) || p.NameRu.StartsWith(TbSearch.Text))).ToList();
            var t = (_filtered
                .Join(DbManager.Local.Categories, p => p.Category, ct => ct.Key, (p, ct) => new
                {
                    p.RcfId,
                    p.FideId,
                    p.NameEng,
                    p.NameRu,
                    p.Birth,
                    Category = ct.Value
                })).ToList();
            listView.ItemsSource = t;
        }

        private void BtnSearchClear_Click(object sender, RoutedEventArgs e)
        {
            TbSearch.Text = "";
            BtnSearch_Click(sender, e);
        }

        private void BtnMGrChange_Click(object sender, RoutedEventArgs e)
        {
            var wnd = new WndMassGroupChange();
            wnd.Closed += (o, args) => { Init(); };
            wnd.ShowDialog();
        }

        private void BtnView_Click(object sender, RoutedEventArgs e)
        {
            if (listView.SelectedIndex==-1) return;
            var wnd = new WndProfileView();
            wnd.Closed += (o, args) => { Init(); };
            wnd.Init(_filtered[listView.SelectedIndex]);
            wnd.ShowDialog();
           
        }

        private async void BtnDel_Click(object sender, RoutedEventArgs e)
        {
            var pr = _filtered[listView.SelectedIndex];
            if (MessageBox.Show(
                    "Удалить профиль:\nРШФ: " + pr.RcfId + "\nFIDE: " + pr.FideId + "\n" + pr.NameRu + "\n" + pr.NameEng, "Удалить",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes) return;
            await DbManager.Local.DeleteProfile(pr);
            Init();
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
