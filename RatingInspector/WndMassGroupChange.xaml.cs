﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using Microsoft.Win32;
using OfficeOpenXml;

namespace RatingInspector
{
    /// <summary>
    /// Логика взаимодействия для WndMassGroupChange.xaml
    /// </summary>
    public partial class WndMassGroupChange : Window
    {
        private List<string> _log = new List<string>();
        public WndMassGroupChange()
        {
            InitializeComponent();
            TbIdList.TextWrapping = TextWrapping.Wrap;
            TbIdList.AcceptsReturn = true;
            CbGroup.ItemsSource = DbManager.Local.Categories.Values;
            CbGroup.SelectedIndex = 0;
        }

        private void BtnAddGroup_Click(object sender, RoutedEventArgs e)
        {
            var wnd = new WndAddCategory();
            wnd.Closed += (o, args) => { CbGroup.ItemsSource = DbManager.Local.Categories.Values; };
            wnd.Show();
        }

        private void BtnLoad_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new OpenFileDialog
            {
                Filter = "Supported files (*.txt *.xlsx)|*.txt;*.xlsx"
            };
            if (dlg.ShowDialog() != true) return;
            if (dlg.FileName.EndsWith("txt"))
            {
                TbIdList.Text = File.ReadAllText(dlg.FileName);
                return;
            }
            if (!dlg.FileName.EndsWith("xlsx")) return;
            try
            {
                var tp = new ExcelPackage(new FileInfo(dlg.FileName));
                tp.Dispose();
            }
            catch (Exception)
            {
                MessageBox.Show("Файл уже открыт в другой программе. Закройте его и повторите попытку", "Невозможно открыть файл", MessageBoxButton.OK);
                return;
            }
            var p = new ExcelPackage(new FileInfo(dlg.FileName));
            var ws = p.Workbook.Worksheets[1];
            var i = ws.Dimension.Start.Column;
            for (var j = ws.Dimension.Start.Row + 1; j <= ws.Dimension.End.Row; j++)
            {
                TbIdList.Text += ws.Cells[j, i].Text + "\n";
            }
            p.Dispose();
        }

        private async void button_Click(object sender, RoutedEventArgs e)
        {
            var profiles = await DbManager.Local.GetProfiles();
            var gr = DbManager.Local.Categories.Keys.ElementAt(CbGroup.SelectedIndex);
            var lines = new List<string>();
            for (var i = 0; i < TbIdList.LineCount; i++)
            {
                var s = TbIdList.GetLineText(i).Trim();
                if (s!="")
                    lines.Add(s);
            }
            var isFide = RbFide.IsChecked != null && (bool)RbFide.IsChecked;
            profiles = isFide ? profiles.Where(p => lines.Contains(p.FideId.ToString())).ToList() : profiles.Where(p => lines.Contains(p.RcfId.ToString())).ToList();
            profiles.ForEach(async p=>
            {
                p.Category = gr;
                await DbManager.Local.WriteProfile(p);
            });
            _log.Add($"Updated {profiles.Count} profiles");
            var c = 0;
            if (isFide)
                lines.ForEach(async l =>
                {
                    if (profiles.Any(p => p.FideId.ToString() == l)) return;
                    _log.Add($"{l} not found. Writed temporary profile.");
                    await DbManager.Local.WriteTempProfile(l, true, gr);
                    c++;
                });
            else
                lines.ForEach(async l =>
                {
                    if (profiles.Any(p => p.RcfId.ToString() == l)) return;
                    _log.Add($"{l} not found. Writed temporary profile.");
                    await DbManager.Local.WriteTempProfile(l, false, gr);
                    c++;
                });
            MessageBox.Show($"Изменено {profiles.Count} профилей.\nСоздано {c} временных профилей.",
                "Обновление данных", MessageBoxButton.OK);
            File.WriteAllLines("Log.txt",_log);
            System.Diagnostics.Process.Start("Log.txt");
        }
    }
}