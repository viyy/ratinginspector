﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Threading.Tasks;
using RatingInspector.DB.Models;
using RatingInspector.Properties;

namespace RatingInspector.DB
{
	public class LocalDbWorker
	{
		private bool _connected;
		private SQLiteConnection _con;

		public static void CreateLocalDb(string path)
		{
			SQLiteConnection.CreateFile(path);
			var con = new SQLiteConnection("Data Source=" + path + ";Version=3;");
			con.Open();
			var sql = Resources.LOCAL_DB_CREATE;
			var command = new SQLiteCommand(sql, con);
			command.ExecuteNonQuery();
			con.Close();
		}

		public void Connect(string path)
		{
			_con = new SQLiteConnection("Data Source=" + path + ";Version=3;");
			_con.Open();
			_connected = true;
			new SQLiteCommand("PRAGMA journal_mode=OFF;", _con).ExecuteNonQuery();
			new SQLiteCommand("PRAGMA synchronous=OFF;", _con).ExecuteNonQuery();
		}

		public string LastUpdate
		{
			get
			{
				if (!_connected) Connect(Conf.DbPath + "/" + Conf.LocalDb);
				var sql = new SQLiteCommand("select Last_update from 'Info';", _con);
				var res = sql.ExecuteReader();
				res.Read();
				return res["Last_update"].ToString();
			}
		}

		public string TmpCount
		{
			get
			{
				if (!_connected) Connect(Conf.DbPath + "/" + Conf.LocalDb);
				var sql = new SQLiteCommand("select count(*) from 'Temp';", _con);
				var res = sql.ExecuteScalar();
				return res.ToString();
			}
		}
		public string Count
		{
			get
			{
				if (!_connected) Connect(Conf.DbPath + "/" + Conf.LocalDb);
				var sql = new SQLiteCommand("select count(*) from 'List';", _con);
				var res = sql.ExecuteScalar();
				return res.ToString();
			}
		}

		public void SetLastUpdateToNow()
		{
			if (!_connected) Connect(Conf.DbPath + "/" + Conf.LocalDb);
			new SQLiteCommand("update 'Info' set Last_update = '" + DateTime.Now.ToString("d") + "';", _con).ExecuteNonQuery();
		}

		public Dictionary<int, string> Categories
		{
			get
			{
				var result = new Dictionary<int, string>();
				if (!_connected) Connect(Conf.DbPath + "/" + Conf.LocalDb);
				var sql = new SQLiteCommand("select * from 'Categories';", _con);
				var res = sql.ExecuteReader();
				while (res.Read())
				{
					result.Add(
						int.Parse(res["Id"].ToString()),
						res["Title"].ToString());
				}
				return result;
			}
		}

	    public async Task WriteTempProfile(MProfile profile)
	    {
	        var sql = new SQLiteCommand("insert into 'Temp' ('Rcf_id', 'Fide_id', 'Category') values (@rcf, @fide, @cat);", _con);
	        sql.Parameters.Add(new SQLiteParameter("@rcf", profile.RcfId));
	        sql.Parameters.Add(new SQLiteParameter("@fide", profile.FideId));
	        sql.Parameters.Add(new SQLiteParameter("@cat", profile.Category));
	        await sql.ExecuteNonQueryAsync();
	    }
		public async Task WriteTempProfile(string id, bool isFide, int cat)
		{
			if (isFide)
			{
				var sql =
					new SQLiteCommand("insert into 'Temp' ('Fide_id', 'Category') values ('"+id+"', '"+cat+"');", _con);
				await sql.ExecuteNonQueryAsync();
			}
			else
			{
				var sql =
					new SQLiteCommand("insert into 'Temp' ('Rcf_id', 'Category') values ('" + id + "', '" + cat + "');", _con);
				await sql.ExecuteNonQueryAsync();
			}
		}

		public async Task<List<MProfile>> GetTempProfiles()
		{
			var list = new List<MProfile>();
			var sql = new SQLiteCommand("select * from Temp;",_con);
			var r = await sql.ExecuteReaderAsync();
			await Task.Run(() =>
			{
				while (r.Read())
				{
					list.Add(new MProfile {RcfId = r.GetInt32(r.GetOrdinal("Rcf_id")), FideId = r.GetInt32(r.GetOrdinal("Fide_id")), Category = r.GetInt32(r.GetOrdinal("Category"))});
				}
			});
			return list;
		}

		public async Task WriteProfile(MProfile pr)
		{
			var sql = _con.CreateCommand();
			sql.CommandText = "update \'List\' set Fide_id=@FideId, Rcf_id=@RcfId, Name_eng=@NameEng, Name_ru=@NameRu, Birth=@Birth, "
				+ "Fed=@Fed, Region=@Region, Fide_rtg=@FideRtg, Fide_rpd=@FideRpd, Fide_blz=@FideBlz, " 
				+ "Rcf_rtg=@RcfRtg, Rcf_Rpd=@RcfRpd, Rcf_blz=@RcfBlz, "
				+ "Category=@Category where id=@Id;";
			sql.Parameters.Add(new SQLiteParameter("@FideId", pr.FideId));
			sql.Parameters.Add(new SQLiteParameter("@RcfId", pr.RcfId));
			sql.Parameters.Add(new SQLiteParameter("@NameEng", pr.NameEng));
			sql.Parameters.Add(new SQLiteParameter("@NameRu", pr.NameRu));
			sql.Parameters.Add(new SQLiteParameter("@Birth", pr.Birth));
			sql.Parameters.Add(new SQLiteParameter("@Fed", pr.Fed));
			sql.Parameters.Add(new SQLiteParameter("@Region", pr.Region));
			sql.Parameters.Add(new SQLiteParameter("@FideRtg", pr.FideRtg));
			sql.Parameters.Add(new SQLiteParameter("@FideRpd", pr.FideRpd));
			sql.Parameters.Add(new SQLiteParameter("@FideBlz", pr.FideBlz));
			sql.Parameters.Add(new SQLiteParameter("@RcfRtg", pr.RcfRtg));
			sql.Parameters.Add(new SQLiteParameter("@RcfRpd", pr.RcfRpd));
			sql.Parameters.Add(new SQLiteParameter("@RcfBlz", pr.RcfBlz));
			sql.Parameters.Add(new SQLiteParameter("@Category", pr.Category));
			sql.Parameters.Add(new SQLiteParameter("@Id", pr.Id));
			if (await sql.ExecuteNonQueryAsync() != 0) return;
			sql.CommandText = "insert into \'List\' " 
				+ "(Fide_id, Rcf_id, Name_eng, Name_ru, Birth, Fed, Region, Fide_rtg, Fide_rpd, Fide_blz, Rcf_rtg, Rcf_rpd, Rcf_blz, Category) " 
				+ "values (@FideId, @RcfId, @NameEng, @NameRu, @Birth, @Fed, @Region, @FideRtg, @FideRpd, @FideBlz, @RcfRtg, @RcfRpd, @RcfBlz, @Category)";
			await sql.ExecuteNonQueryAsync();
		}
		public async Task<List<MProfile>> GetProfiles()
		{
			var list = new List<MProfile>();
			var sql = new SQLiteCommand("select * from List;", _con);
			var r = await sql.ExecuteReaderAsync();
			await Task.Run(() =>
			{
				var ords = new []
				{
					r.GetOrdinal("id"),
					r.GetOrdinal("Fide_id"),
					r.GetOrdinal("Rcf_id"),
					r.GetOrdinal("Name_eng"),
					r.GetOrdinal("Name_ru"),
					r.GetOrdinal("Birth"),
					r.GetOrdinal("Fed"),
					r.GetOrdinal("Region"),
					r.GetOrdinal("Fide_rtg"),
					r.GetOrdinal("Fide_rpd"),
					r.GetOrdinal("Fide_blz"),
					r.GetOrdinal("Rcf_rtg"),
					r.GetOrdinal("Rcf_rpd"),
					r.GetOrdinal("Rcf_blz"),
					r.GetOrdinal("Category")
				};
				while (r.Read())
				{
					list.Add(new MProfile
					{
						Id = r.GetInt32(ords[0]),
						FideId = r.GetInt32(ords[1]),
						RcfId = r.GetInt32(ords[2]),
						NameEng = r.GetString(ords[3]),
						NameRu = r.GetString(ords[4]),
						Birth = r.GetInt32(ords[5]),
						Fed = r.GetString(ords[6]),
						Region = r.GetInt32(ords[7]),
						FideRtg = r.GetInt32(ords[8]),
						FideRpd = r.GetInt32(ords[9]),
						FideBlz = r.GetInt32(ords[10]),
						RcfRtg = r.GetInt32(ords[11]),
						RcfRpd = r.GetInt32(ords[12]),
						RcfBlz = r.GetInt32(ords[13]),
						Category = r.GetInt32(ords[14])
					});
				}
			});
			return list;
		}

	    public async Task DeleteProfile(MProfile profile)
	    {
	        var sql = new SQLiteCommand("delete from \'List\' where id=@Id;", _con);
	        sql.Parameters.Add(new SQLiteParameter("@Id", profile.Id));
	        await sql.ExecuteNonQueryAsync();
        }

		public async Task DeleteTempList()
		{
			var sql = new SQLiteCommand("delete from 'Temp';", _con);
			await sql.ExecuteNonQueryAsync();
		}

		public void AddCategory(string title)
		{
			new SQLiteCommand("insert into 'Categories'('Title') values ('" + title + "');", _con).ExecuteNonQuery();
		}

		public void Close()
		{
			_connected = false;
		    _con.Close();
		    _con = null;
		    GC.Collect();
		    GC.WaitForPendingFinalizers();
        }
	}
}