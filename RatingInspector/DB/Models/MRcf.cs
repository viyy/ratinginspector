﻿namespace RatingInspector.DB.Models
{
	public class MRcf
	{
		public int Rcfid { get; set; }
		public int Fideid { get; set; }
		public string Name { get; set; }
		public int Birth { get; set; }
		public int Region { get; set; }
		public int Rtg { get; set; }
		public int Rpd { get; set; }
		public int Blz { get; set; }
	}
}