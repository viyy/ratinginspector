﻿namespace RatingInspector.DB.Models
{
	public class MProfile
	{
		public int Id { get; set; }
		public int FideId { get; set; }
		public int RcfId { get; set; }
		public string NameEng { get; set; } = "";
		public string NameRu { get; set; } = "";
		public string ExportName { get; set; } = "";
		public int Birth { get; set; }
		public string Fed { get; set; } = "";
		public int Region { get; set; }
		public int FideRtg { get; set; }
		public int FideRpd { get; set; }
		public int FideBlz { get; set; }
		public int RcfRtg { get; set; }
		public int RcfRpd { get; set; }
		public int RcfBlz { get; set; }
		public int Category { get; set; }

		public void Merge(MProfile pr)
		{
			Id = pr.Id != 0 ? pr.Id : Id;
			FideId = pr.FideId != 0 ? pr.FideId : FideId;
			RcfId = pr.RcfId != 0 ? pr.RcfId : RcfId;
			NameEng = pr.NameEng != "" ? pr.NameEng : NameEng;
			NameRu = pr.NameRu != "" ? pr.NameRu : NameRu;
			Birth = pr.Birth != 0 ? pr.Birth : Birth;
			Fed = pr.Fed != "" ? pr.Fed : Fed;
			Region = pr.Region != 0 ? pr.Region : Region;
			FideRtg = pr.FideRtg != 0 ? pr.FideRtg : FideRtg;
			FideRpd = pr.FideRpd != 0 ? pr.FideRpd : FideRpd;
			FideBlz = pr.FideBlz != 0 ? pr.FideBlz : FideBlz;
			RcfRtg = pr.RcfRtg != 0 ? pr.RcfRtg : RcfRtg;
			RcfRpd = pr.RcfRpd != 0 ? pr.RcfRpd : RcfRpd;
			RcfBlz = pr.RcfBlz != 0 ? pr.RcfBlz : RcfBlz;
			Category = pr.Category != 0 ? pr.Category : Category;
		}

		public void FillExportName(int namePriority = 0) //Rus by default
		{
			ExportName = namePriority == 0 ? (NameRu != "" ? NameRu : NameEng) : (NameEng != "" ? NameEng : NameRu);
		}
	}
}