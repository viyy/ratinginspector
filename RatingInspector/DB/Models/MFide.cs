﻿namespace RatingInspector.DB.Models
{
	public class MFide
	{
		public int Fid { get; set; }
		public int Birth { get; set; }
		public string Name { get; set; }
		public string Fed { get; set; }
		public int Rtg { get; set; }
		public int Rpd { get; set; }
		public int Blz { get; set; }
	}
}