﻿using System;
using System.Data.SQLite;
using System.Threading.Tasks;
using RatingInspector.DB.Models;
using RatingInspector.Properties;

namespace RatingInspector.DB
{
	public class GlobalDbWorker
	{
		private bool _connected;
		private SQLiteConnection _con;

		public void Close()
		{
			_connected = false;
			_con.Close();
		    GC.Collect();
		    GC.WaitForPendingFinalizers();
        }

		public string FideLastUpdate
		{
			get
			{
				if (!_connected) Connect(Conf.DbPath+"/"+Conf.GlobalDb);
				var sql = new SQLiteCommand("select Fide_last_update from 'Info';", _con);
				var res = sql.ExecuteReader();
				res.Read();
				return res["Fide_last_update"].ToString();
			}
		}
		public string RcfLastUpdate
		{
			get
			{
				if (!_connected) Connect(Conf.DbPath + "/" + Conf.GlobalDb);
				var sql = new SQLiteCommand("select Rcf_last_update from 'Info';", _con);
				var res = sql.ExecuteReader();
				res.Read();
				return res["Rcf_last_update"].ToString();
			}
		}
		public void SetFideLastUpdateToNow()
		{
			if (!_connected) Connect(Conf.DbPath + "/" + Conf.GlobalDb);
			new SQLiteCommand("update 'Info' set Fide_last_update = '" + DateTime.Now.ToString("d") + "';", _con).ExecuteNonQuery();
		}
		public void SetRcfLastUpdateToNow()
		{
			if (!_connected) Connect(Conf.DbPath + "/" + Conf.GlobalDb);
			new SQLiteCommand("update 'Info' set Rcf_last_update = '" + DateTime.Now.ToString("d") + "';", _con).ExecuteNonQuery();
		}
		public async Task WriteProfile(MFide profile)
		{
			await Task.Run(async () =>
			{
				var sql =
					new SQLiteCommand(
						"Update FIDE set Name=@Name, Rtg=@Rtg, Rpd=@Rpd, Blz=@Blz where Fide_id=@Id;", _con);
				sql.Parameters.Add(new SQLiteParameter("@Name", profile.Name));
				sql.Parameters.Add(new SQLiteParameter("@Rtg", profile.Rtg));
				sql.Parameters.Add(new SQLiteParameter("@Rpd", profile.Rpd));
				sql.Parameters.Add(new SQLiteParameter("@Blz", profile.Blz));
				sql.Parameters.Add(new SQLiteParameter("@Id", profile.Fid));

				if (await sql.ExecuteNonQueryAsync() != 0) return;
				sql =
					new SQLiteCommand(
						"insert into FIDE (Fide_id, Name, Birth, Fed, Rtg, Rpd, Blz) Values (@Id, @Name, @Birth, @Fed, @Rtg, @Rpd, @Blz);", _con);
				sql.Parameters.Add(new SQLiteParameter("@Birth", profile.Birth));
				sql.Parameters.Add(new SQLiteParameter("@Fed", profile.Fed));
				sql.Parameters.Add(new SQLiteParameter("@Name", profile.Name));
				sql.Parameters.Add(new SQLiteParameter("@Rtg", profile.Rtg));
				sql.Parameters.Add(new SQLiteParameter("@Rpd", profile.Rpd));
				sql.Parameters.Add(new SQLiteParameter("@Blz", profile.Blz));
				sql.Parameters.Add(new SQLiteParameter("@Id", profile.Fid));
				await sql.ExecuteNonQueryAsync();
			});
		}

		public async Task WriteProfile(MRcf profile)
		{
			await Task.Run(async () =>
			{
				var sql =
					new SQLiteCommand(
						"Update RCF set Name=@name, Rtg=@rtg, Rpd=@rpd, Blz=@blz, Fide_id=@fide where Rcf_id=@rcf;", _con);
			    sql.Parameters.Add(new SQLiteParameter("@name", profile.Name));
			    sql.Parameters.Add(new SQLiteParameter("@rtg", profile.Rtg));
			    sql.Parameters.Add(new SQLiteParameter("@rpd", profile.Rpd));
			    sql.Parameters.Add(new SQLiteParameter("@blz", profile.Blz));
			    sql.Parameters.Add(new SQLiteParameter("@fide", profile.Fideid));
			    sql.Parameters.Add(new SQLiteParameter("@rcf", profile.Rcfid));
                if (await sql.ExecuteNonQueryAsync() != 0) return;
				sql =
					new SQLiteCommand(
						"insert into RCF (Rcf_id, Fide_id, Name, Birth, Region, Rtg, Rpd, Blz) Values (@rcf, @fide, @name, @birth, @region, @rtg, @rpd, @blz);", _con);
			    sql.Parameters.Add(new SQLiteParameter("@name", profile.Name));
			    sql.Parameters.Add(new SQLiteParameter("@rtg", profile.Rtg));
			    sql.Parameters.Add(new SQLiteParameter("@rpd", profile.Rpd));
			    sql.Parameters.Add(new SQLiteParameter("@blz", profile.Blz));
			    sql.Parameters.Add(new SQLiteParameter("@fide", profile.Fideid));
			    sql.Parameters.Add(new SQLiteParameter("@rcf", profile.Rcfid));
			    sql.Parameters.Add(new SQLiteParameter("@birth", profile.Birth));
			    sql.Parameters.Add(new SQLiteParameter("@region", profile.Region));
                await sql.ExecuteNonQueryAsync();
			});
		}

		public async Task<MProfile> GetProfileByFide(int id)
		{
			var sql=new SQLiteCommand("select * from 'FIDE' where Fide_id=@Id;",_con);
			sql.Parameters.Add(new SQLiteParameter("@Id", id));
			var r = await sql.ExecuteReaderAsync();
			if (!r.HasRows) return new MProfile();
			if (r.Read())
			{
				return new MProfile
				{
					FideId = r.GetInt32(r.GetOrdinal("Fide_id")),
					NameEng = r.GetString(r.GetOrdinal("Name")),
					Birth = r.GetInt32(r.GetOrdinal("Birth")),
					Fed = r.GetString(r.GetOrdinal("Fed")),
					FideRtg = r.GetInt32(r.GetOrdinal("Rtg")),
					FideRpd = r.GetInt32(r.GetOrdinal("Rpd")),
					FideBlz = r.GetInt32(r.GetOrdinal("Blz"))
				};
			}
			return null;
		}

		public async Task<int> GetRcfIdByFide(int fideId)
		{
			var sql = new SQLiteCommand("select Rcf_id from RCF where Fide_id=@Fid;", _con);
			sql.Parameters.Add(new SQLiteParameter("@Fid", fideId));
			var r = await sql.ExecuteReaderAsync();
			if (!r.HasRows) return 0;
			return !r.Read() ? 0 : r.GetInt32(r.GetOrdinal("Rcf_id"));
		}
	    public async Task<MProfile> GetRcfProfileByFide(int id)
	    {
	        var sql = new SQLiteCommand("select * from 'RCF' where Fide_id=" + id + ";", _con);
	        var r = await sql.ExecuteReaderAsync();
	        if (!r.HasRows) return new MProfile();
	        if (!r.Read()) return new MProfile();
	        var ords = new[] { r.GetOrdinal("Rcf_id"), r.GetOrdinal("Fide_id"), r.GetOrdinal("Name"), r.GetOrdinal("Birth"), r.GetOrdinal("Region"), r.GetOrdinal("Rtg"), r.GetOrdinal("Rpd"), r.GetOrdinal("Blz") };
	        return new MProfile
	        {
	            RcfId = r.GetInt32(ords[0]),
	            FideId = r.GetInt32(ords[1]),
	            NameRu = r.GetString(ords[2]),
	            Birth = r.GetInt32(ords[3]),
	            Region = r.GetInt32(ords[4]),
	            RcfRtg = r.GetInt32(ords[5]),
	            RcfRpd = r.GetInt32(ords[6]),
	            RcfBlz = r.GetInt32(ords[7])
	        };
	    }
        public async Task<MProfile> GetProfileByRcf(int id)
		{
			var sql = new SQLiteCommand("select * from 'RCF' where Rcf_id=" + id + ";", _con);
			var r = await sql.ExecuteReaderAsync();
			if (!r.HasRows) return new MProfile();
			if (!r.Read()) return new MProfile();
			var ords = new[] { r.GetOrdinal("Rcf_id") , r.GetOrdinal("Fide_id"), r.GetOrdinal("Name"), r.GetOrdinal("Birth"), r.GetOrdinal("Region"), r.GetOrdinal("Rtg") , r.GetOrdinal("Rpd"), r.GetOrdinal("Blz") };
			return new MProfile
			{
				RcfId = r.GetInt32(ords[0]),
				FideId = r.GetInt32(ords[1]),
				NameRu = r.GetString(ords[2]),
				Birth = r.GetInt32(ords[3]),
				Region = r.GetInt32(ords[4]),
				RcfRtg = r.GetInt32(ords[5]),
				RcfRpd = r.GetInt32(ords[6]),
				RcfBlz = r.GetInt32(ords[7])
			};
		}

		public void Connect(string path)
		{
			_con = new SQLiteConnection("Data Source=" + path + ";Version=3;");
			_con.Open();
			_connected = true;
			new SQLiteCommand("PRAGMA journal_mode=OFF;", _con).ExecuteNonQuery();
			new SQLiteCommand("PRAGMA synchronous=OFF;", _con).ExecuteNonQuery();
		}
		public static void CreateGlobalDb(string path)
		{
			SQLiteConnection.CreateFile(path);
			var con = new SQLiteConnection("Data Source="+path+";Version=3;");
			con.Open();
			var sql = Resources.GLOBAL_DB_CREATE;
			var command = new SQLiteCommand(sql, con);
			command.ExecuteNonQuery();
			con.Close();
		}
	}
}