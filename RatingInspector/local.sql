--
-- File generated with SQLiteStudio v3.1.0 on �� ��� 27 16:11:49 2017
--
-- Text encoding used: System
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: Temp
CREATE TABLE "Temp" (Fide_id INTEGER, Rcf_id INTEGER);

-- Table: List
CREATE TABLE List (id INTEGER PRIMARY KEY AUTOINCREMENT, Fide_id INTEGER, Rcf_id INTEGER, Name_eng TEXT, Name_ru TEXT, Birth INTEGER, Fed TEXT, Region INTEGER, Fide_rtg INTEGER DEFAULT (0), Fide_rpd INTEGER DEFAULT (0), Fide_blz INTEGER DEFAULT (0), Rcf_rtg INTEGER DEFAULT (0), Rcf_rpd INTEGER DEFAULT (0), Rcf_blz INTEGER DEFAULT (0), Category INTEGER);

-- Table: Categories
CREATE TABLE Categories (Id INTEGER PRIMARY KEY AUTOINCREMENT, Title TEXT);

-- Table: Info
CREATE TABLE Info (Last_update TEXT, Version INTEGER);

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
