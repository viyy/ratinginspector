﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using RatingInspector.DB.Models;

namespace RatingInspector
{
    /// <summary>
    /// Логика взаимодействия для WndTempProfileEditor.xaml
    /// </summary>
    public partial class WndTempProfileEditor : Window
    {
        private Dictionary<int, string> Categories = DbManager.Local.Categories;
        private WndTempProfilesView Master;
        private int index;
        public WndTempProfileEditor()
        {
            InitializeComponent();
            comboBox.ItemsSource = Categories;
        }
        public void FillData(int index0, int rcfId, int fideId, int group, WndTempProfilesView master)
        {
            TbRcfId.Text = rcfId.ToString();
            TbFideId.Text = fideId.ToString();
            comboBox.SelectedIndex = Categories.Keys.ToList().IndexOf(group);
            Master = master;
            index = index0;
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            TbRcfId.Background = new SolidColorBrush(Color.FromRgb(255, 255, 255));
            TbFideId.Background = new SolidColorBrush(Color.FromRgb(255, 255, 255));
            if (!int.TryParse(TbRcfId.Text, out var rcf))
            {
                TbRcfId.Background = new SolidColorBrush(Color.FromRgb(255,150,150));
                return;
            }
            if (!int.TryParse(TbFideId.Text, out var fide))
            {
                TbFideId.Background = new SolidColorBrush(Color.FromRgb(255, 150, 150));
                return;
            }
            Master.Update(index, new MProfile{RcfId = rcf, FideId = fide, Category = Categories.ElementAt(comboBox.SelectedIndex).Key});
            Close();
        }
    }
}
