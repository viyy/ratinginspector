--
-- File generated with SQLiteStudio v3.1.0 on �� ��� 27 16:00:56 2017
--
-- Text encoding used: System
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: FIDE
CREATE TABLE FIDE (Fide_id INTEGER UNIQUE, Name TEXT, Birth INTEGER, Fed TEXT, Rtg INTEGER, Rpd INTEGER, Blz INTEGER);

-- Table: Info
CREATE TABLE Info (Fide_last_update TEXT, Rcf_last_update TEXT, Fide_version INTEGER, Rcf_version INTEGER);

-- Table: RCF
CREATE TABLE RCF (Rcs_id INTEGER UNIQUE, Fide_id INTEGER UNIQUE, Name TEXT, Birth INTEGER, Region INTEGER, Rtg INTEGER, Rpd INTEGER, Blz INTEGER);

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
