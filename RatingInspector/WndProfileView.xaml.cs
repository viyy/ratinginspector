﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Win32;
using RatingInspector.DB.Models;

namespace RatingInspector
{
    /// <summary>
    /// Логика взаимодействия для WndProfileView.xaml
    /// </summary>
    public partial class WndProfileView : Window
    {
        private MProfile _mProfile = null;
        public WndProfileView()
        {
            InitializeComponent();
        }

        public void Init(MProfile profile)
        {
            _mProfile = profile;
            LblFideId.Content = _mProfile.FideId;
            LblRcfId.Content = _mProfile.RcfId;
            LblNameRu.Content = _mProfile.NameRu;
            LblNameEng.Content = _mProfile.NameEng;
            listView.Items.Add(new
            {
                _mProfile.RcfRtg,
                _mProfile.RcfRpd,
                _mProfile.RcfBlz,
                _mProfile.FideRtg,
                _mProfile.FideRpd,
                _mProfile.FideBlz
            });
            LblBirth.Content = _mProfile.Birth;
            CbGroup.ItemsSource = DbManager.Local.Categories.Values;
            CbGroup.SelectedIndex = DbManager.Local.Categories.Keys.ToList().IndexOf(_mProfile.Category);
            CbGroup.SelectionChanged += (sender, args) => { BtnSave.IsEnabled = true; };
        }

        private void BtnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private async void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            _mProfile.Category = DbManager.Local.Categories.Keys.ElementAt(CbGroup.SelectedIndex);
            await DbManager.Local.WriteProfile(_mProfile);
            Lbl_Saved.Content = "Сохранено";
        }

        private void BtnCopy_Click(object sender, RoutedEventArgs e)
        {
            var str = "Имя:".PadRight(10) + _mProfile.NameRu + Environment.NewLine;
            str += "Имя(En):".PadRight(10) + _mProfile.NameEng + Environment.NewLine;
            str += "Г.р.:".PadRight(10) + _mProfile.Birth + Environment.NewLine;
            str += "РШФ Id:".PadRight(10) + _mProfile.RcfId + Environment.NewLine;
            str += "Fide Id:".PadRight(10) + _mProfile.FideId + Environment.NewLine;
            str += "РШФ Std:".PadRight(10) + _mProfile.RcfRtg + Environment.NewLine;
            str += "РШФ Rpd:".PadRight(10) + _mProfile.RcfRpd + Environment.NewLine;
            str += "РШФ Blz:".PadRight(10) + _mProfile.RcfBlz + Environment.NewLine;
            str += "FIDE Std:".PadRight(10) + _mProfile.FideRtg + Environment.NewLine;
            str += "FIDE Rpd:".PadRight(10) + _mProfile.FideRpd + Environment.NewLine;
            str += "FIDE Blz:".PadRight(10) + _mProfile.FideBlz + Environment.NewLine;
            Clipboard.SetText(str);
            Lbl_Saved.Content = "Скопировано";
        }
    }
}
