﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Win32;
using OfficeOpenXml;
using OfficeOpenXml.Table;

namespace RatingInspector
{
    /// <summary>
    /// Логика взаимодействия для WndListExport.xaml
    /// </summary>
    public partial class WndListExport : Window
    {
        public WndListExport()
        {
            InitializeComponent();
            CbName.SelectionChanged += CbName_SelectionChanged;
            TbList.TextWrapping = TextWrapping.Wrap;
            TbList.AcceptsReturn = true;
        }

        private void CbName_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (CbName.SelectedIndex != 2)
            {
                CbSort.IsEnabled = true;
                return;
            }
            CbSort.SelectedIndex = 2;
            CbSort.IsEnabled = false;
        }

        private async void Btn_export_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new SaveFileDialog
            {
                FileName = "Rtg" + DateTime.Today.ToShortDateString(),
                Filter = "*.xlsx|*.xlsx",
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
            };
            if (!dlg.ShowDialog() == true) return;
            var filepath = dlg.FileName;
            var list = await DbManager.Local.GetProfiles();
            var lines = new List<string>();
            for (var i = 0; i < TbList.LineCount; i++)
            {
                lines.Add(TbList.GetLineText(i).Trim());
            }
            var isFide = RbFide.IsChecked != null && (bool)RbFide.IsChecked;
            list = isFide ? list.Where(p => lines.Contains(p.FideId.ToString())).ToList() : list.Where(p => lines.Contains(p.RcfId.ToString())).ToList();
            if (CbName.SelectedIndex != 2)
                list.ForEach(p => p.FillExportName(CbName.SelectedIndex));

            var xlsx = new ExcelPackage(new FileInfo(filepath));
            var ws = xlsx.Workbook.Worksheets.Add("RTG-" + DateTime.Today.Day + "-" + DateTime.Today.Month + "-" +
                                                  DateTime.Today.Year);
            //Headers
            var headersFlags = new Dictionary<string, CExportHeader>
            {
                {"RcfId", new CExportHeader{IsNeeded = CbRid.IsChecked??false, Title = "РШФ Id"}},
                {"FideId", new CExportHeader{IsNeeded = CbFid.IsChecked??false, Title = "FIDE Id"} },
                {"NameRu", new CExportHeader{IsNeeded = CbSort.SelectedIndex==2, Title = "Имя (Ru)"} },
                {"NameEng", new CExportHeader{IsNeeded = CbSort.SelectedIndex==2, Title = "Имя (En)"}  },
                {"NameExp", new CExportHeader{IsNeeded = CbSort.SelectedIndex!=2, Title = "Имя"}  },
                {"Fed", new CExportHeader{IsNeeded = CbCountry.IsChecked??false, Title = "Страна"} },
                {"Birth", new CExportHeader{IsNeeded = CbBirth.IsChecked??false, Title = "Г.Р."} },
                {"Reg", new CExportHeader{IsNeeded = CbRegion.IsChecked??false, Title = "Регион"} },
                {"FRtg",new CExportHeader{IsNeeded = CbFrtg.IsChecked??false, Title = "Fide Rtg"} },
                {"FRpd",new CExportHeader{IsNeeded = CbFrpd.IsChecked??false, Title = "Fide Rpd"} },
                {"FBlz",new CExportHeader{IsNeeded = CbFblz.IsChecked??false, Title = "Fide Blz"} },
                {"RRtg",new CExportHeader{IsNeeded = CbRrtg.IsChecked??false, Title = "РШФ Rtg"} },
                {"RRpd",new CExportHeader{IsNeeded = CbRrpd.IsChecked??false, Title = "РШФ Rpd"} },
                {"RBlz",new CExportHeader{IsNeeded = CbRblz.IsChecked??false, Title = "РШФ Blz"} },
                {"Cat", new CExportHeader{IsNeeded = true, Title = "Группа"}}
            };
            var col = 1;
            foreach (var header in headersFlags)
            {
                if (!header.Value.IsNeeded) continue;
                ws.Cells[1, col].Value = header.Value.Title;
                col++;
            }
            switch (CbSort.SelectedIndex)
            {
                case 0:
                    list = list.OrderBy(p => p.Category).ThenBy(p => p.ExportName).ToList();
                    break;
                case 1:
                    list = list.OrderBy(p => p.ExportName).ToList();
                    break;
                default:
                    list = list.OrderBy(p => p.Category).ToList();
                    break;
            }
            var row = 2;
            var cats = DbManager.Local.Categories;
            //data
            foreach (var profile in list)
            {
                col = 1;
                if (headersFlags["RcfId"].IsNeeded)
                {
                    ws.Cells[row, col].Value = profile.RcfId;
                    col++;
                }
                if (headersFlags["FideId"].IsNeeded)
                {
                    ws.Cells[row, col].Value = profile.FideId;
                    col++;
                }
                if (headersFlags["NameRu"].IsNeeded)
                {
                    ws.Cells[row, col].Value = profile.NameRu;
                    col++;
                }
                if (headersFlags["NameEng"].IsNeeded)
                {
                    ws.Cells[row, col].Value = profile.NameEng;
                    col++;
                }
                if (headersFlags["NameExp"].IsNeeded)
                {
                    ws.Cells[row, col].Value = profile.ExportName;
                    col++;
                }
                if (headersFlags["Fed"].IsNeeded)
                {
                    ws.Cells[row, col].Value = profile.Fed;
                    col++;
                }
                if (headersFlags["Birth"].IsNeeded)
                {
                    ws.Cells[row, col].Value = profile.Birth;
                    col++;
                }
                if (headersFlags["Reg"].IsNeeded)
                {
                    ws.Cells[row, col].Value = profile.Region;
                    col++;
                }
                if (headersFlags["FRtg"].IsNeeded)
                {
                    ws.Cells[row, col].Value = profile.FideRtg;
                    col++;
                }
                if (headersFlags["FRpd"].IsNeeded)
                {
                    ws.Cells[row, col].Value = profile.FideRpd;
                    col++;
                }
                if (headersFlags["FBlz"].IsNeeded)
                {
                    ws.Cells[row, col].Value = profile.FideBlz;
                    col++;
                }
                if (headersFlags["RRtg"].IsNeeded)
                {
                    ws.Cells[row, col].Value = profile.RcfRtg;
                    col++;
                }
                if (headersFlags["RRpd"].IsNeeded)
                {
                    ws.Cells[row, col].Value = profile.RcfRpd;
                    col++;
                }
                if (headersFlags["RBlz"].IsNeeded)
                {
                    ws.Cells[row, col].Value = profile.RcfBlz;
                    col++;
                }
                if (headersFlags["Cat"].IsNeeded)
                {
                    ws.Cells[row, col].Value = cats[profile.Category];
                }
                row++;
            }
            var range = ws.Cells[ws.Dimension.Start.Row, ws.Dimension.Start.Column, ws.Dimension.End.Row, ws.Dimension.End.Column];
            var table = ws.Tables.Add(range, "table1");
            range.AutoFitColumns();
            table.TableStyle = TableStyles.Light1;
            xlsx.Save();
            System.Diagnostics.Process.Start(filepath);
        }

        private void Btn_Cancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void BtnLoad_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new OpenFileDialog
            {
                Filter = "Supported files (*.txt *.xlsx)|*.txt;*.xlsx"
            };
            if (dlg.ShowDialog() != true) return;
            if (dlg.FileName.EndsWith("txt"))
            {
                TbList.Text = File.ReadAllText(dlg.FileName);
                return;
            }
            if (!dlg.FileName.EndsWith("xlsx")) return;
            try
            {
                var tp = new ExcelPackage(new FileInfo(dlg.FileName));
                tp.Dispose();
            }
            catch (Exception)
            {
                MessageBox.Show("Файл уже открыт в другой программе. Закройте его и повторите попытку", "Невозможно открыть файл", MessageBoxButton.OK);
                return;
            }
            var p = new ExcelPackage(new FileInfo(dlg.FileName));
            var ws = p.Workbook.Worksheets[1];
            var i = ws.Dimension.Start.Column;
            for (var j = ws.Dimension.Start.Row; j <= ws.Dimension.End.Row; j++)
            {
                TbList.Text += ws.Cells[j, i].Text + "\n";
            }
            p.Dispose();
        }
    }
}
