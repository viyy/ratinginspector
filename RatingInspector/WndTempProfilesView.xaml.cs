﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using RatingInspector.DB.Models;

namespace RatingInspector
{
    /// <summary>
    /// Логика взаимодействия для WndTempProfilesView.xaml
    /// </summary>
    public partial class WndTempProfilesView : Window
    {
        public List<MProfile> Profiles = new List<MProfile>();
        public List<MProfile> Filtered = new List<MProfile>();
        public WndTempProfilesView()
        {
            InitializeComponent();
            listView.SelectionMode = SelectionMode.Single;
            Init();
        }

        private async void Init()
        {
            Profiles = await DbManager.Local.GetTempProfiles();
            Filtered = Profiles;
            var t = (Filtered.Join(DbManager.Local.Categories, p => p.Category, ct => ct.Key, (p, ct) => new
            {
                p.RcfId,
                p.FideId,
                Category = ct.Value
            })).ToList();
            listView.ItemsSource = t;
        }

        private void BtnSearch_Click(object sender, RoutedEventArgs e)
        {
            Filtered = Profiles.Where(p => (p.RcfId.ToString().StartsWith(TbSearchBox.Text) ||
                             p.FideId.ToString().StartsWith(TbSearchBox.Text))).ToList();
            var t = (Filtered
                .Join(DbManager.Local.Categories, p => p.Category, ct => ct.Key, (p, ct) => new
                {
                    RowId = p.Id,
                    p.RcfId,
                    p.FideId,
                    Category = ct.Value
                })).ToList();
            listView.ItemsSource = t;
        }

        private void BtnDelSearch_Click(object sender, RoutedEventArgs e)
        {
            TbSearchBox.Text = "";
            BtnSearch_Click(sender,e);
        }

        public void Update(int index0, MProfile pr)
        {
            IsEnabled = true;
            var i = Profiles.IndexOf(Filtered[index0]);
            Profiles[i] = pr;
            Filtered = Profiles.Where(p => (p.RcfId.ToString().StartsWith(TbSearchBox.Text) ||
                                            p.FideId.ToString().StartsWith(TbSearchBox.Text))).ToList();
            var t = (Filtered
                .Where(p => (p.RcfId.ToString().StartsWith(TbSearchBox.Text) ||
                             p.FideId.ToString().StartsWith(TbSearchBox.Text)))
                .Join(DbManager.Local.Categories, p => p.Category, ct => ct.Key, (p, ct) => new
                {
                    RowId = p.Id,
                    p.RcfId,
                    p.FideId,
                    Category = ct.Value
                })).ToList();
            listView.ItemsSource = t;
        }

        private void BtnEdit_Click(object sender, RoutedEventArgs e)
        {
            if (listView.SelectedIndex == -1) return;
            IsEnabled = false;
            var wnd = new WndTempProfileEditor();
            wnd.Show();
            var p = Filtered.ElementAt(listView.SelectedIndex);
            wnd.FillData(listView.SelectedIndex, p.RcfId, p.FideId, p.Category, this);
            wnd.Closed += (o, args) =>
            {
                IsEnabled = true;
            };
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (listView.SelectedIndex == -1) return;
            var i = listView.SelectedIndex;
            var str =
                $"Вы точно хотите удалить эту запись?\nРШФ Id: {Filtered[i].RcfId}\nFide Id: {Filtered[i].FideId}";
            if (MessageBox.Show(str, "Удаление", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                Profiles.RemoveAt(Profiles.IndexOf(Filtered[i]));
            Filtered = Profiles.Where(p => (p.RcfId.ToString().StartsWith(TbSearchBox.Text) ||
                                            p.FideId.ToString().StartsWith(TbSearchBox.Text))).ToList();
            var t = (Filtered
                .Where(p => (p.RcfId.ToString().StartsWith(TbSearchBox.Text) ||
                             p.FideId.ToString().StartsWith(TbSearchBox.Text)))
                .Join(DbManager.Local.Categories, p => p.Category, ct => ct.Key, (p, ct) => new
                {
                    RowId = p.Id,
                    p.RcfId,
                    p.FideId,
                    Category = ct.Value
                })).ToList();
            listView.ItemsSource = t;
        }

        private async void button_Click(object sender, RoutedEventArgs e)
        {
            IsEnabled = false;
            await DbManager.Local.DeleteTempList();
            await Task.WhenAll(Profiles.Select(pr => DbManager.Local.WriteTempProfile(pr)));
            Init();
            IsEnabled = true;
            Close();
        }
    }
}
