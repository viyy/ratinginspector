﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RatingInspector
{
	/// <summary>
	/// Interaction logic for WndAddCategory.xaml
	/// </summary>
	public partial class WndAddCategory : Window
	{
		public WndAddCategory()
		{
			InitializeComponent();
		}

		private void button_Click(object sender, RoutedEventArgs e)
		{
			Close();
		}

		private void button1_Click(object sender, RoutedEventArgs e)
		{
			if (textBox.Text.Trim() == "") return;
			DbManager.Local.AddCategory(textBox.Text.Trim());
			Close();
		}
	}
}
