﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Win32;
using OfficeOpenXml;

namespace RatingInspector
{
	/// <summary>
	/// Interaction logic for WndImport.xaml
	/// </summary>
	public partial class WndImport : Window
	{
		public WndImport()
		{
			InitializeComponent();
			tb_ids.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
		}

		private void btn_addCat_Click(object sender, RoutedEventArgs e)
		{
			new WndAddCategory().ShowDialog();
		}

		private void cb_cats_DropDownOpened(object sender, EventArgs e)
		{
			cb_cats.ItemsSource = DbManager.Local.Categories;
		}

		private void btn_clear_Click(object sender, RoutedEventArgs e)
		{
			tb_ids.Clear();
		}

		private void btn_openfile_Click(object sender, RoutedEventArgs e)
		{
			var dlg = new OpenFileDialog
			{
				Filter = "Supported files (*.txt *.xlsx)|*.txt;*.xlsx"
			};
			if (dlg.ShowDialog() != true) return;
			if (dlg.FileName.EndsWith("txt"))
			{
				tb_ids.Text = File.ReadAllText(dlg.FileName);
				return;
			}
			if (!dlg.FileName.EndsWith("xlsx")) return;
		    try
		    {
		        var tp = new ExcelPackage(new FileInfo(dlg.FileName));
                tp.Dispose();
		    }
		    catch (Exception)
		    {
		        MessageBox.Show("Файл уже открыт в другой программе. Закройте его и повторите попытку", "Невозможно открыть файл", MessageBoxButton.OK);
		        return;
            }
		    var p = new ExcelPackage(new FileInfo(dlg.FileName));
            var ws = p.Workbook.Worksheets[1];
			var i = ws.Dimension.Start.Column;
			for (var j = ws.Dimension.Start.Row; j <= ws.Dimension.End.Row; j++)
			{
				tb_ids.Text += ws.Cells[j, i].Text + "\n";
			}
			p.Dispose();
		}

		private async void btn_import_Click(object sender, RoutedEventArgs e)
		{
			cb_cats.IsEnabled = false;
			rbtn_fide.IsEnabled = false;
			rbtn_rcf.IsEnabled = false;
			tb_ids.IsEnabled = false;
			var isFide = rbtn_fide.IsChecked != null && (bool) rbtn_fide.IsChecked;
			var id = (int)cb_cats.SelectedValue;
			var lines = new List<string>();
		    for (var i = 0; i < tb_ids.LineCount; i++)
		    {
		        var s = tb_ids.GetLineText(i).Trim();
		        if (s != "")
		            lines.Add(s);
		    }
            lines = lines.Where(s => s.Trim() != "").Select(s=>s.Trim()).ToList();
			await Task.WhenAll(lines.Select(l=>DbManager.Local.WriteTempProfile(l,isFide, id)));
			cb_cats.IsEnabled = true;
			rbtn_fide.IsEnabled = true;
			rbtn_rcf.IsEnabled = true;
			tb_ids.IsEnabled = true;
			MessageBox.Show("Импорт завершен", "Импорт", MessageBoxButton.OK, MessageBoxImage.Information);
		}
	}
}
