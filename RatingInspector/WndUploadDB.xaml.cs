﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Windows;
using System.Xml.Linq;

namespace RatingInspector
{
    /// <summary>
    /// Логика взаимодействия для WndUploadDB.xaml
    /// </summary>
    public partial class WndUploadDB : Window
    {

        private string _host;
        private string _port;
        private string _user;
        private string _pass;
        private string _dir;

        public WndUploadDB()
        {
            InitializeComponent();
            TaskUpload();
        }

        private void TaskUpload()
        {
            if (!File.Exists("admin"))
            {
                LblStatus.Content = "Нет прав для операции.";
                return;
            }
            var cfg = XDocument.Load("admin");
            if (cfg.Root != null) _host = cfg.Root.Element("Host")?.Value.Trim();
            if (cfg.Root != null) _port = cfg.Root.Element("Port")?.Value.Trim();
            if (cfg.Root != null) _user = cfg.Root.Element("User")?.Value.Trim();
            if (cfg.Root != null) _pass = cfg.Root.Element("Pass")?.Value.Trim();
            if (cfg.Root != null) _dir = cfg.Root.Element("Dir")?.Value.Trim();
            if (string.IsNullOrEmpty(_pass))
            {
                LblStatus.Content = "Некорректный пароль";
                return;
            }
            _pass = Encoding.UTF8.GetString(Convert.FromBase64String(_pass));

            // Get the object used to communicate with the server.
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create($"ftp://{_host}:{_port}/{_dir}/local_srv.zip");
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.KeepAlive = true;
            request.UseBinary = true;

            // This example assumes the FTP site uses anonymous logon.
            request.Credentials = new NetworkCredential(_user, _pass);

            // Copy the contents of the file to the request stream.
            FileStream fs = File.OpenRead(Conf.DbPath + "/" + Conf.LocalDb);
            byte[] buffer = new byte[fs.Length];
            fs.Read(buffer, 0, buffer.Length);
            fs.Close();
            request.ContentLength = buffer.Length;

            Stream ftpstream = request.GetRequestStream();
            ftpstream.Write(buffer, 0, buffer.Length);
            ftpstream.Close();
            FtpWebResponse response = (FtpWebResponse)request.GetResponse();

            LblStatus.Content = $"Upload File Complete, status {response.StatusDescription}";

            response.Close();
        }

    }
}
