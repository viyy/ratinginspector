﻿using System.Xml.Linq;

namespace RatingInspector
{
	public class Conf
	{
		public static string FideUrl { set; get; }
		public static string RcfUrl { set; get; }
		public static string GlobalDb { get; set; }
		public static string LocalDb { get; set; }
		public static string DownloadPath { get; set; }
		public static string DbPath { get; set; }
        public static string Server { get; set; }
	    public static string Dir { get; set; }
        public static string Api { get; set; }

		public static void Load(string path = "Settings.xml")
		{
			var cfg = XDocument.Load("Settings.xml");
			if (cfg.Root != null) FideUrl = cfg.Root.Element("FideUrl")?.Value.Trim();
			if (cfg.Root != null) RcfUrl = cfg.Root.Element("RcfUrl")?.Value.Trim();
			if (cfg.Root != null) GlobalDb = cfg.Root.Element("GlobalDb")?.Value.Trim();
			if (cfg.Root != null) LocalDb = cfg.Root.Element("LocalDb")?.Value.Trim();
			if (cfg.Root != null) DownloadPath = cfg.Root.Element("DownloadPath")?.Value.Trim();
			if (cfg.Root != null) DbPath = cfg.Root.Element("DbPath")?.Value.Trim();
		    if (cfg.Root != null) Server = cfg.Root.Element("Server")?.Value.Trim();
		    if (cfg.Root != null) Dir = cfg.Root.Element("Dir")?.Value.Trim();
            if (cfg.Root != null) Api = cfg.Root.Element("Api")?.Value.Trim();
        }

	    public static void Save(string path = "Settings.xml")
	    {
	        var cfg = new XDocument();
            cfg.Add(new XElement("Settings"));
	        if (cfg.Root == null) return;
            cfg.Root.Add(new XElement("FideUrl", FideUrl));
	        cfg.Root.Add(new XElement("RcfUrl", RcfUrl));
	        cfg.Root.Add(new XElement("GlobalDb", GlobalDb));
	        cfg.Root.Add(new XElement("LocalDb", LocalDb));
	        cfg.Root.Add(new XElement("DownloadPath", DownloadPath));
	        cfg.Root.Add(new XElement("DbPath", DbPath));
	        cfg.Root.Add(new XElement("Server", Server));
	        cfg.Root.Add(new XElement("Dir", Dir));
            cfg.Root.Add(new XElement("Api", Api));
            cfg.Save(path);

        }
	}
}