﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Windows;
using System.Xml.Linq;
using OfficeOpenXml;
using RatingInspector.DB;
using RatingInspector.DB.Models;

namespace RatingInspector
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary> 
    // ReSharper disable once InheritdocConsiderUsage
    public partial class MainWindow
    {
		private bool _busy;

		public MainWindow()
		{
			InitializeComponent();
			Conf.Load();
			CheckFolders();
			CheckDb();
			DbManager.Global = new GlobalDbWorker();
			DbManager.Global.Connect(Conf.DbPath+"/"+Conf.GlobalDb);
			DbManager.Local = new LocalDbWorker();
			DbManager.Local.Connect(Conf.DbPath+"/"+Conf.LocalDb);
		    if (!File.Exists("admin"))
		    {
		        BtnUploadDB.IsEnabled = false;
		        BtnUploadDB.Visibility = Visibility.Hidden;
		        button.IsEnabled = false;
		        button.Visibility = Visibility.Hidden;
		    }
		    else
		    {
		        BtnDlDb.IsEnabled = false;
		        BtnDlDb.Visibility = Visibility.Hidden;
		    }
			lbl_fidelastupd.Content = DbManager.Global.FideLastUpdate;
			lbl_rcflastupd.Content = DbManager.Global.RcfLastUpdate;
			lbl_lastupd.Content = DbManager.Local.LastUpdate;
			lbl_tmpcount.Content = DbManager.Local.TmpCount;
			lbl_count.Content = DbManager.Local.Count;
		}

		private static void CheckDb()
		{
			if (!File.Exists(Conf.DbPath + "/" + Conf.GlobalDb))
				GlobalDbWorker.CreateGlobalDb(Conf.DbPath+"/"+Conf.GlobalDb);
			if (!File.Exists(Conf.DbPath + "/" + Conf.LocalDb))
				LocalDbWorker.CreateLocalDb(Conf.DbPath + "/" + Conf.LocalDb);
		}

		private static void CheckFolders()
		{
			if (!Directory.Exists(Conf.DownloadPath))
				Directory.CreateDirectory(Conf.DownloadPath);
			if (!Directory.Exists(Conf.DbPath))
				Directory.CreateDirectory(Conf.DbPath);
		}

		private void client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
		{
			var bytesIn = double.Parse(e.BytesReceived.ToString());
			var totalBytes = double.Parse(e.TotalBytesToReceive.ToString());
			var percentage = bytesIn / totalBytes * 100;
			lbl_status.Content = "Dl: " + e.BytesReceived + " / " + e.TotalBytesToReceive;
			// ReSharper disable once SpecifyACultureInStringConversionExplicitly
			prb_status.Value = int.Parse(Math.Truncate(percentage).ToString());
		}

		private void client_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
		{
			lbl_status.Content = "Completed";
		}

		private async Task StartTaskDownload(string url, string path)
		{
			if (_busy) return;
			_busy = true;
			lbl_status.Content = "Starting Download: "+path.Split('/')[path.Split('/').Length-1];
			var client = new WebClient();
			client.DownloadProgressChanged += client_DownloadProgressChanged;
			client.DownloadFileCompleted += client_DownloadFileCompleted;
			await client.DownloadFileTaskAsync(new Uri(url), path);
			_busy = false;
		}

		private async Task FideXmlToModel()
		{
			var profiles = new List<MFide>();
			await Task.Run(() =>
            {
                int i;
                var xdoc = XDocument.Load(Conf.DownloadPath + "/players_list_xml_foa.xml");

                /*foreach (var pl in xdoc.Descendants("player"))
			    {
			        if (pl.Element("country")?.Value != "RUS") continue;
			        var pr = new MFide
			        {
			            Fid = int.TryParse(pl.Element("fideid")?.Value, out i) ? i : 0,
			            Name = pl.Element("name")?.Value,
			            Fed = pl.Element("country")?.Value,
			            Birth = int.TryParse(pl.Element("birthday")?.Value, out i) ? i : 0,
			            Rtg = int.TryParse(pl.Element("rating")?.Value, out i) ? i : 0,
			            Rpd = int.TryParse(pl.Element("rapid_rating")?.Value, out i) ? i : 0,
			            Blz = int.TryParse(pl.Element("blitz_rating")?.Value, out i) ? i : 0
			        };
			        await DbManager.Global.WriteProfile(pr);
			    }*/

                profiles = (from p in xdoc.Descendants("player")
                            where p.Element("country")?.Value == "RUS"
                            select new MFide
                            {
                                Fid = int.TryParse(p.Element("fideid")?.Value, out i) ? i : 0,
                                Name = p.Element("name")?.Value,
                                Fed = p.Element("country")?.Value,
                                Birth = int.TryParse(p.Element("birthday")?.Value, out i) ? i : 0,
                                Rtg = int.TryParse(p.Element("rating")?.Value, out i) ? i : 0,
                                Rpd = int.TryParse(p.Element("rapid_rating")?.Value, out i) ? i : 0,
                                Blz = int.TryParse(p.Element("blitz_rating")?.Value, out i) ? i : 0
                            }).ToList();
            });
			await Task.WhenAll(profiles.Select( pr=> DbManager.Global.WriteProfile(pr)));
			profiles.Clear();
		}

		private async Task HandleFide()
		{
			if (_busy) return;
			lbl_status.Content = "Unzipping...";
			_busy = true;
			await Task.Run(() =>
			{
				if (File.Exists(Conf.DownloadPath + "/players_list_xml_foa.xml")) File.Delete(Conf.DownloadPath + "/players_list_xml_foa.xml");
				System.IO.Compression.ZipFile.ExtractToDirectory(Conf.DownloadPath+"/fide.zip", Conf.DownloadPath);
			});
			lbl_status.Content = "Writing...";
			await FideXmlToModel();
			DbManager.Global.SetFideLastUpdateToNow();
		    GC.Collect();
		    GC.WaitForPendingFinalizers();
            _busy = false;
			lbl_status.Content = "Idle";
		}

		private async Task HandleRcf()
		{
			if (_busy) return;
			lbl_status.Content = "Reading RCF profiles...";
			_busy = true;
			var profiles = new List<MRcf>();
			await Task.Run(() =>
			{
				var p = new ExcelPackage(new FileInfo(Conf.DownloadPath+"/rcf.xlsx"));
				var ws = p.Workbook.Worksheets[1];
				var i = ws.Dimension.Start.Column;
				for (var j = ws.Dimension.Start.Row + 1; j <= ws.Dimension.End.Row; j++)
				{
					profiles.Add(new MRcf
					{
						Rcfid = ws.Cells[j, i].GetValue<int>(),
						Fideid = ws.Cells[j, i+1].GetValue<int>(),
						Region = ws.Cells[j, i+2].GetValue<int>(),
						Name = ws.Cells[j, i + 3].Text.Trim(),
						Birth = ws.Cells[j, i+5].GetValue<int>(),
						Rtg= ws.Cells[j, i+6].GetValue<int>(),
						Rpd = ws.Cells[j, i+8].GetValue<int>(),
						Blz = ws.Cells[j, i+10].GetValue<int>()
					});
				}
			});
			lbl_status.Content = "Writing...";
			await Task.WhenAll(profiles.Select(pr => DbManager.Global.WriteProfile(pr)));
			profiles.Clear();
			DbManager.Global.SetRcfLastUpdateToNow();
			_busy = false;
			lbl_status.Content = "Idle";
		}

		private async void btn_updfide_Click(object sender, RoutedEventArgs e)
		{
			await StartTaskDownload(Conf.FideUrl,Conf.DownloadPath+"/fide.zip");
			await HandleFide();
			lbl_fidelastupd.Content = DbManager.Global.FideLastUpdate;
			Directory.EnumerateFiles(Conf.DownloadPath).ToList().ForEach(File.Delete);
		}

		private async void btn_updrcf_Click(object sender, RoutedEventArgs e)
		{
			await StartTaskDownload(Conf.RcfUrl, Conf.DownloadPath + "/rcf.xlsx");
			await HandleRcf();
			lbl_rcflastupd.Content = DbManager.Global.RcfLastUpdate;
			Directory.EnumerateFiles(Conf.DownloadPath).ToList().ForEach(File.Delete);
		}

		private async void btn_updall_Click(object sender, RoutedEventArgs e)
		{
			await StartTaskDownload(Conf.FideUrl, Conf.DownloadPath + "/fide.zip");
			await StartTaskDownload(Conf.RcfUrl, Conf.DownloadPath + "/rcf.xlsx");
			await HandleFide();
			await HandleRcf();
			lbl_fidelastupd.Content = DbManager.Global.FideLastUpdate;
			lbl_rcflastupd.Content = DbManager.Global.RcfLastUpdate;
			Directory.EnumerateFiles(Conf.DownloadPath).ToList().ForEach(File.Delete);
		}

		private void btn_exit_Click(object sender, RoutedEventArgs e)
		{
			DbManager.Global.Close();
			DbManager.Local.Close();
			Application.Current.Shutdown();
		}

		private void btn_makeAll_Click(object sender, RoutedEventArgs e)
		{
			btn_updall_Click(sender, e);
			btn_update_Click(sender, e);
			btn_exp_Click(sender, e);
		}

		private void button_Click(object sender, RoutedEventArgs e)
		{
			var impwnd = new WndImport();
			impwnd.Closed += (o, args) => { lbl_tmpcount.Content = DbManager.Local.TmpCount; };
			impwnd.ShowDialog();
		}

		private async void btn_update_Click(object sender, RoutedEventArgs e)
		{
			if (_busy) return;
			_busy = true;
			lbl_status.Content = "Receiving Profiles...";
			var tmplist = await DbManager.Local.GetTempProfiles();
			var list = await DbManager.Local.GetProfiles();
			lbl_status.Content = "Merge Profiles...";
            tmplist = tmplist.Where(p => !list.Any(x => (p.FideId == x.FideId && p.FideId!=0) || (p.RcfId==x.RcfId && p.RcfId!=0))).ToList();
			list.AddRange(tmplist);
			foreach (var t in list)
			{
				if (t.RcfId != 0)
				{
					t.Merge(await DbManager.Global.GetProfileByRcf(t.RcfId));
				}
			    if (t.FideId == 0) continue;
			    t.Merge(await DbManager.Global.GetProfileByFide(t.FideId));
			    t.Merge(await DbManager.Global.GetRcfProfileByFide(t.FideId));
			}
			list.ForEach(async p =>
			{
				if (p.FideId != 0 && p.RcfId == 0)
					p.RcfId = await DbManager.Global.GetRcfIdByFide(p.FideId);
			});
			var resultList = new List<MProfile>();
			lbl_status.Content = "Updating Profiles...";
			foreach (var pr1 in list)
			{
				var duplicatefound = false;
				foreach (var _ in resultList.Where(pr2 => (pr1.FideId != 0)&&(pr1.FideId == pr2.FideId)||(pr2.RcfId!=0)&&(pr1.RcfId==pr2.RcfId)))
					duplicatefound = true;
				if (!duplicatefound)
					resultList.Add(pr1);
			}
			lbl_status.Content = "Writing Profiles...";
			await Task.WhenAll(resultList.Select(pr => DbManager.Local.WriteProfile(pr)));
			lbl_status.Content = "Clearing Temp Profiles...";
			await DbManager.Local.DeleteTempList();
			DbManager.Local.SetLastUpdateToNow();
			lbl_lastupd.Content = DbManager.Local.LastUpdate;
			lbl_tmpcount.Content = DbManager.Local.TmpCount;
			lbl_count.Content = DbManager.Local.Count;
			lbl_status.Content = "Idle";
			_busy = false;
		}

		private void btn_exp_Click(object sender, RoutedEventArgs e)
		{
			new WndExport().ShowDialog();
		}

        private void btn_tmplook_Click(object sender, RoutedEventArgs e)
        {
            var impwnd = new WndTempProfilesView();
            impwnd.Closed += (o, args) => { lbl_tmpcount.Content = DbManager.Local.TmpCount; };
            impwnd.ShowDialog();
        }

        private void btn_look_Click(object sender, RoutedEventArgs e)
        {
            var impwnd = new WndProfilesView();
            impwnd.Closed += (o, args) => { lbl_count.Content = DbManager.Local.Count; };
            impwnd.ShowDialog();
        }

        private void BtnListExport_Click(object sender, RoutedEventArgs e)
        {
            new WndListExport().ShowDialog();
        }

        private void BtnUploadDB_Click(object sender, RoutedEventArgs e)
        {
            DbManager.Local.Close();
            var wnd = new WndUploadDB();
            wnd.Closed += (o, args) =>
            {
                DbManager.Local.Connect(Conf.DbPath + "/" + Conf.LocalDb);
            };
            wnd.ShowDialog();
        }

        private async void BtnDownloadDB(object sender, RoutedEventArgs e)
        {
            if (_busy) return;
            _busy = true;
            lbl_status.Content = "Отключаем базу...";
            DbManager.Local.Close();
            lbl_status.Content = "Создаем резервную копию...";
            if (File.Exists(Conf.DbPath + "/" + Conf.LocalDb + ".bak"))
            {
                File.Move(Conf.DbPath + "/" + Conf.LocalDb + ".bak", Conf.DbPath + "/" + Conf.LocalDb + ".bak2");
            }
            var f = false;
            while (!f)
            {
                try
                {
                    File.Move(Conf.DbPath + "/" + Conf.LocalDb, Conf.DbPath + "/" + Conf.LocalDb + ".bak");
                    f = true;
                }
                catch (Exception)
                {
                    lbl_status.Content = "Создаем резервную копию (2)...";
                }
            }
            if (File.Exists(Conf.DbPath + "/" + Conf.LocalDb + ".bak2"))
                File.Delete(Conf.DbPath + "/" + Conf.LocalDb + ".bak2");
            _busy = false;
            await StartTaskDownload("http://"+Conf.Server + "/"+Conf.Dir+"/local_srv.zip", Conf.DbPath + "/" + Conf.LocalDb);
            _busy = true;
            DbManager.Local.Connect(Conf.DbPath + "/" + Conf.LocalDb);
            lbl_lastupd.Content = DbManager.Local.LastUpdate;
            lbl_tmpcount.Content = DbManager.Local.TmpCount;
            lbl_count.Content = DbManager.Local.Count;
            _busy = false;
        }
    }
}
