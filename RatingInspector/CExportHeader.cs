﻿namespace RatingInspector
{
	public class CExportHeader
	{
		public bool IsNeeded { get; set; }
		public string Title { get; set; } = "";
	}

}